from django.shortcuts import get_object_or_404, redirect, render
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountCategoryForm


@login_required
def ultimate_receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"ultimate_receipt_list": receipt_list}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt_list = form.save(False)
            receipt_list.purchaser = request.user
            receipt_list.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(
        owner=request.user,
    ).distinct()
    context = {
        "category": category,
    }
    return render(request, "categories/category_list.html", context)


@login_required
def account_list(request):
    category = Account.objects.filter(
        owner=request.user,
    ).distinct()
    context = {
        "category": category,
    }
    return render(request, "accounts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "categories/create.html", context)



@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("account_list")
    else:
        form = AccountCategoryForm()
    context = {"form": form}
    return render(request, "accounts/create.html", context)
